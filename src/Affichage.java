package src;

import java.awt.Color;
import src.graph;

import javax.swing.*;


public class Affichage {
	private JFrame window;
	private int count = 80;
	
	public Affichage()
	{
		//creation fenetre
		window = new JFrame("Load-balancer Client - AUDINOT CASTILLE"); // fenetre principale
		window.setSize(550, 120);  // taille  fenetre
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // cliquer sur le bouton ferme la fenetre
		window.setLocationRelativeTo(null); // fenetre centr�e sur l'�cran
		window.setVisible(true);
		//ecriture des trois noms des serveurs
		graph g1 = new graph();
		graph g2 = new graph();
		graph g3 = new graph();
		//ecriture serveur 1
		g1.editRect( 10,  20,  "Serv 1", Color.blue);
		window.setContentPane(g1);
		window.setVisible(true);
		try{Thread.sleep(100);} // timer pour laisser le temps d'afficher
		catch(Exception e)
		{System.out.println(e);}
		//ecriture serveur 2
		g2.editRect( 10,  40,  "Serv 2", Color.red);
		window.setContentPane(g2);
		window.setVisible(true);
		try{Thread.sleep(100);}// timer pour laisser le temps d'afficher
		catch(Exception e)
		{System.out.println(e);}
		//ecriture serveur 3
		g3.editRect( 10,  60,  "Serv 3", Color.black);
		window.setContentPane(g3);
		window.setVisible(true);
		try{Thread.sleep(100);}// timer pour laisser le temps d'afficher
		catch(Exception e)
		{System.out.println(e);}
	}
	public void editGraph(int serv1, int serv2, int serv3)
	{
		int x = count;
		// rectangle server 1
		graph g1 = new graph();
		g1.editRect(x+20,20,serv1, Color.blue);
		window.setContentPane(g1);
		window.setVisible(true);
		try{Thread.sleep(100);}// timer pour laisser le temps d'afficher
		catch(Exception e)
		{System.out.println(e);}
		//rectangle server 2
		graph g2 = new graph();
		g2.editRect(x+20,40,serv2, Color.red);
		window.setContentPane(g2);
		window.setVisible(true);
		try{Thread.sleep(100);}// timer pour laisser le temps d'afficher
		catch(Exception e)
		{System.out.println(e);}
		//rectangle server3
		graph g3 = new graph();
		g3.editRect(x+20,60,serv3, Color.black);
		window.setContentPane(g3);
		window.setVisible(true);
		try{Thread.sleep(50);}// timer pour laisser le temps d'afficher
		catch(Exception e)
		{System.out.println(e);}
		// d�calage pour la prochaine vague
		this.count = this.count + 40;
	}
}
