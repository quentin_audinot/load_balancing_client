package src;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class HttpConnection extends NotifyingThread {
	
	String url;
	String sourceCode;
	int server;
	
	public HttpConnection(String url) {
		this.url = url;
	}
	
	public void get() throws Exception {
		URL url = new URL(this.url);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");

		int responseCode = connection.getResponseCode();

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		//in.close();
		connection.disconnect();
		this.sourceCode = response.toString();
		
		Document doc = Jsoup.parse(sourceCode);
		Element paragraph = doc.select("p").first();
		String pText = paragraph.html().substring(15, 16);
		
		this.server = Integer.parseInt(pText);
	}
	
	public void doRun() {
		try {
			get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getServer() {
		return this.server;
	}
}
