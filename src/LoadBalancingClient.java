package src;

import java.util.ArrayList;

public class LoadBalancingClient implements ThreadCompleteListener {
	
	String url;
	ArrayList<HttpConnection> connections;
	static int threadsCompleted = 0;
	boolean allThreadsCompleted = false;
	
	int nbServer1, nbServer2, nbServer3;
	
	public LoadBalancingClient(String url, int nbThreads) {
		this.url = url;
		this.connections = new ArrayList<HttpConnection>();
		this.nbServer1 = 0;
		this.nbServer2 = 0;
		this.nbServer3 = 0;
		
		HttpConnection hc;
		
		for (int i=0; i<nbThreads; i++) {
			hc = new HttpConnection(url);
			hc.addListener(this);
			this.connections.add(hc);
		}
	}
	
	public void launch() {
		for (int i=0; i<connections.size(); i++) {
			try
			{
				Thread.sleep(30); // on met 30ms entre chaque requetes pour �viter la surcharge du serveur Apache
				connections.get(i).start();
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			
		}
	}
	
	@Override
	public synchronized void notifyOfThreadComplete(Thread thread) {
		HttpConnection t = (HttpConnection) thread;

		switch (t.getServer()) {
		case 1:
			this.nbServer1++;
			break;
		case 2:
			this.nbServer2++;
			break;
		case 3:
			this.nbServer3++;
			break;
		default:
			break;
		}

		threadsCompleted++;
		if (threadsCompleted >= this.connections.size())
			this.allThreadsCompleted = true;
		
	}
	
	public static void main(String[] args) {
		Affichage window = new Affichage();
		for (int i = 0 ; i < 10 ; i++)//on va lancer 10 vagues de requetes
		{
			//ouverture de connection et pr�paration de 200 requetes
			LoadBalancingClient lbc = new LoadBalancingClient("http://localhost:8080/unpacked", 200);
			//lancement des requetes
			lbc.launch();
			// on attend que toutes les requetes soient termin�es
			while (!lbc.allThreadsCompleted) {
				System.out.print("");
			}
			// affichage en console et appel de la fonction d'affichage graphique
			System.out.println("Vague : "+i);
			System.out.println("Serveur 1 : " + lbc.nbServer1);
			System.out.println("Serveur 2 : " + lbc.nbServer2);
			System.out.println("Serveur 3 : " + lbc.nbServer3);
			window.editGraph(lbc.nbServer1, lbc.nbServer2, lbc.nbServer3);
			// on laisse au serveur le temps d'�vacuer les requetes non trait�es
			try{
				Thread.sleep(1000);
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		
	}	
}
