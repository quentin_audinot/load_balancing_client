package src;

public interface ThreadCompleteListener {
	void notifyOfThreadComplete(final Thread thread);
}
