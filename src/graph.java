package src;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class graph extends JPanel{
	private int x = 20;
	private int y = 20;
	private String value = "0";
	private Color clr = Color.black;
	  public void paintComponent(Graphics g){
		    //Dessine le texte aux emplacement fix�s
		    g.setColor(clr);
		    Font font = new Font("Verdana", Font.PLAIN, 20);//gere la police et la taille
		    g.setFont(font);
		    g.drawString(String.valueOf(value),x,y); // dessine le texte
		  }
	  public void editRect(int x, int y, int value, Color c) // edit depuis les valeur des tests
	  {
		  this.x = x;
		  this.y = y;
		  this.value = Integer.toString(value);
		  this.clr = c;
	  }
	  public void editRect(int x, int y, String value, Color c) // edite pour les noms des serveurs
	  {
		  this.x = x;
		  this.y = y;
		  this.value = value;
		  this.clr = c;
	  }
}
